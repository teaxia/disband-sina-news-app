# 解锁新浪新闻需要下载APP才能查看全文

#### 介绍
解锁微博PC端跳转至新浪页面时，浏览新闻需要强制下载APP的油猴脚本


#### 安装教程

1.  安装油猴脚本（中文安装方法 [https://greasyfork.org/zh-CN](https://greasyfork.org/zh-CN)）
2.  直接安装插件（安装地址[点击跳转](https://greasyfork.org/zh-CN/scripts/419868-%E8%A7%A3%E9%94%81%E6%96%B0%E6%B5%AA%E6%96%B0%E9%97%BB%E9%9C%80%E8%A6%81%E4%B8%8B%E8%BD%BDapp%E6%89%8D%E8%83%BD%E6%9F%A5%E7%9C%8B%E5%85%A8%E6%96%87)）

#### 使用说明

安装后自动使用

#### 其他
如果需要支持新的网站，可以直接提issues

#### 更新记录
21-01-28 1.1.0 增加了顶部、播放器广告过滤